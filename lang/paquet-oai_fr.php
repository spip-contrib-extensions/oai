<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'oai_description' => 'Ce plugin gère la partie "serveur" d’OAI en fournissant un URL à donner à un moissonneur afin qu’il scanne les métadonnées des contenus du site.',
	'oai_nom' => 'Entrepôt OAI',
	'oai_slogan' => 'Fournir un entrepôt OAI pouvant répondre à un moissonneur',
);
